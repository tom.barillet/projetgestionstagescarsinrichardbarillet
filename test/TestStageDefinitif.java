/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Date;
import org.junit.*;
import modele.metier.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.*;

/**
 *
 * @author tomba
 */
public class TestStageDefinitif {

    private Annee uneAnnee;
    private Specialite uneSpecialite;
    private Classe uneClasse;
    private Etudiant unEtudiant;
    private Entreprise uneEntreprise;
    private Referent unReferent;
    private Date uneDateDeb;
    private Date uneDateFin;
    private StageDefinitif unStage;
    private Etudiant unEtudiant2;
    private Enseignant unEnseignant;
    
    public TestStageDefinitif() {
        
    
    }
    
    @Before
    public void setUp() {
        uneAnnee = new Annee(1,"2020");
        uneSpecialite = new Specialite(1,"SLAM");
        uneClasse = new Classe(1,uneSpecialite,uneAnnee,"2SLAM");
        unEtudiant = new Etudiant(1,"Barillet", "Tom","tbarillet@hotmail.fr", "tbarillet","secret",uneClasse);
        uneEntreprise = new Entreprise(1,"Rekilibre", "routeDeClisson","SARL");
        unReferent = new Referent(1, uneEntreprise,"Richard","Mathias","mrichard@gmail.com","0695272489");
        unEnseignant = new Enseignant(0,"BOURGEOIS","Nicolas","nbourgeois@gmail.com","nbourgeois","secret");
        uneDateDeb = new Date(2020,1,5);
        uneDateFin = new Date(2020,3,2);
        unStage = new StageDefinitif(1,uneAnnee,uneEntreprise,unEtudiant,unReferent,unEnseignant,uneDateDeb, uneDateFin,"Amélioration de site web de Rekilibre");
        unEtudiant2 = new Etudiant(2,"Carsin","Pol-Aurelien","pcarsin@la-joliverie.com","pcarsin","secret",uneClasse);
    }
    @Test
    public void testGetEtudiant(){
    assertEquals(unEtudiant, unStage.getlEtudiant());
    assertNotSame(unStage.getlEtudiant(), unEtudiant2);
    assertNotNull(unStage.getlEtudiant());
}
}
