

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import modele.metier.Classe;
import modele.metier.Specialite;
import modele.metier.Annee;
import modele.dao.ConnectionDAO;
import modele.dao.ClasseDAO;
import modele.dao.AnneeDAO;
import modele.dao.EntrepriseDAO;
import modele.dao.EtudiantDAO;
import modele.metier.Entreprise;
import modele.metier.Etudiant;

/**
 *
 * @author btssio
 */
public class TestJdbc {

//    static String sgbd = "oracle";
    static String sgbd = "mysql";
    static String url = "";
    static String utilisateurBD;
    static String motDePasseBD;

    public static void main(String[] args)throws ClassNotFoundException 
    {
        
        try{
            System.out.println("\nTest1 : sélection multiple, résultat dans une collection d'objets de type <Classe>");
            ArrayList<Classe> S1 = ClasseDAO.getAll();
                for (int i = 0; i< S1.size(); i++) {
                    System.out.println(S1.get(i).getNom());} 
            System.out.println("\nTest2 : sélection multiple, résultat dans une collection d'objets de type <Etudiant>");
            ArrayList<Etudiant> E1 = EtudiantDAO.getAll();
                for (int i = 0; i< E1.size(); i++) {
                    System.out.println(E1.get(i).getNom()+" "+ E1.get(i).getPrenom());
                } 
            System.out.println("\nTest3 : sélection unique, retourne un objet de type <Entreprise> ");
            Entreprise uneEntreprise = EntrepriseDAO.getOneById(2);
            System.out.println(uneEntreprise.getNom());
            
            System.out.println("\nTest4 : sélection unique, retourne un objet de type <Etudiant> ");
            Etudiant unEtudiant = EtudiantDAO.getOneById(2);
            System.out.println(unEtudiant.getLaClasse().getIdClasse());
              
        System.out.println("\nTest5 : sélection unique, retourne un objet de type <Classe> ");
            Classe uneClasse = ClasseDAO.getOneById(2);
            System.out.println(uneClasse.getIdClasse());
                }  
        catch (SQLException e) {
            System.err.println("Erreur SQL : " + e); 
        }
        
  
        


        /*java.sql.Connection cnx = null;

        try {
            System.out.println("Test0 : connexion");
            Class.forName("com.mysql.fabric.jdbc.FabricMySQLDriver");
            switch (sgbd) {
                case "mysql":
                    url = "jdbc:mysql://localhost:3306/gestionstage";
                    utilisateurBD = "root";
                    motDePasseBD = "";
                    break;
                case "oracle":
                    // A COMPLETER
                    url = "jdbc:oracle:thin:@localhost:1521:XE";
                    utilisateurBD = "AGENCEB";
                    motDePasseBD = "secret";
                    break;
            }
            cnx = DriverManager.getConnection(url, utilisateurBD, motDePasseBD);
            System.out.println("connexion " + sgbd + " réussie");
            System.out.println("Test0 effectué");

           
            System.out.println("\nTest4 : sélection multiple, résultat dans une collection d'objets de type <Client>");
             String requete;
            ResultSet rs;
            requete = "SELECT * FROM classe INNER JOIN specialite ON classe.idSpe = specialite.idSpe INNER JOIN annee ON classe.idAnnee = annee.idAnnee";
            PreparedStatement pstmt;
            pstmt = cnx.prepareStatement(requete);
            
            rs = pstmt.executeQuery();
            ArrayList<Classe> lesClasses = new ArrayList<>();
            while (rs.next()) {
                Specialite uneSpe = new Specialite(
                        rs.getInt("idSpe"),
                        rs.getString("libelle")
                );
                Annee uneAnnee = new Annee(
                        rs.getInt("idAnnee"),
                        rs.getString("annee")
                
                );
                Classe uneClasse = new Classe(
                        rs.getInt("idClasse"),
                        uneSpe,
                        uneAnnee,
                        rs.getString("nom")
                        
                );
                lesClasses.add(uneClasse);
            }
            for (Classe uneClasse : lesClasses) {
                System.out.println(uneClasse);
            }

            System.out.println("-->Test4 complet");

            

        } catch (SQLException e) {
            System.err.println("Erreur SQL : " + e);
        } finally {
            try {
                if (cnx != null) {
                    cnx.close();
                }
            } catch (SQLException e) {
                System.err.println("Erreur de fermeture de la connexion JDBC : " + e);
            }
        }*/
    }
    }
