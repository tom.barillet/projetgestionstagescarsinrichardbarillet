-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 30 mars 2021 à 15:47
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gestionstage`
--

-- --------------------------------------------------------

--
-- Structure de la table `annee`
--

DROP TABLE IF EXISTS `annee`;
CREATE TABLE IF NOT EXISTS `annee` (
  `idAnnee` int(3) NOT NULL AUTO_INCREMENT,
  `annee` varchar(9) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`idAnnee`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `annee`
--

INSERT INTO `annee` (`idAnnee`, `annee`) VALUES
(1, '2019/2020'),
(2, '2020/2021');

-- --------------------------------------------------------

--
-- Structure de la table `classe`
--

DROP TABLE IF EXISTS `classe`;
CREATE TABLE IF NOT EXISTS `classe` (
  `idClasse` int(3) NOT NULL AUTO_INCREMENT,
  `idSpe` int(3) NOT NULL,
  `idAnnee` int(3) NOT NULL,
  `nom` varchar(25) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`idClasse`),
  KEY `FK_CLASSE_SPE` (`idSpe`),
  KEY `FK_CLASSE_ANNEE` (`idAnnee`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `classe`
--

INSERT INTO `classe` (`idClasse`, `idSpe`, `idAnnee`, `nom`) VALUES
(1, 1, 1, '1SLAM'),
(2, 2, 1, '1SISR'),
(3, 1, 2, '2SLAM'),
(4, 2, 2, '2SISR');

-- --------------------------------------------------------

--
-- Structure de la table `enseignant`
--

DROP TABLE IF EXISTS `enseignant`;
CREATE TABLE IF NOT EXISTS `enseignant` (
  `idEnseignant` int(3) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) COLLATE utf8_bin NOT NULL,
  `prenom` varchar(25) COLLATE utf8_bin NOT NULL,
  `estAdmin` tinyint(1) NOT NULL,
  `mail` varchar(30) COLLATE utf8_bin NOT NULL,
  `login` varchar(25) COLLATE utf8_bin NOT NULL,
  `mdp` varchar(75) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`idEnseignant`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `enseignant`
--

INSERT INTO `enseignant` (`idEnseignant`, `nom`, `prenom`, `estAdmin`, `mail`, `login`, `mdp`) VALUES
(1, 'BOURGEOIS', 'Nicolas', 1, 'nbourgeois@la-joliverie.com', 'nbourgeois', 'ad3fdb03ae4c4d2bbd8e996ab5742820959693e1'),
(2, 'CONTANT', 'Nelly', 1, 'ncontant@la-joliverie.com', 'ncontant', '7e3555be0ee573857fc505fa61ab5019c331ae3d');

-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

DROP TABLE IF EXISTS `entreprise`;
CREATE TABLE IF NOT EXISTS `entreprise` (
  `idEntreprise` int(3) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) COLLATE utf8_bin NOT NULL,
  `Adresse` varchar(25) COLLATE utf8_bin NOT NULL,
  `raisonSociale` varchar(25) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`idEntreprise`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `entreprise`
--

INSERT INTO `entreprise` (`idEntreprise`, `nom`, `Adresse`, `raisonSociale`) VALUES
(1, 'raie qui libre', '', ''),
(2, 'pornic agglomération', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `etudiant`
--

DROP TABLE IF EXISTS `etudiant`;
CREATE TABLE IF NOT EXISTS `etudiant` (
  `idEtudiant` int(3) NOT NULL AUTO_INCREMENT,
  `idClasse` int(3) NOT NULL,
  `nom` varchar(25) COLLATE utf8_bin NOT NULL,
  `prenom` varchar(25) COLLATE utf8_bin NOT NULL,
  `mail` varchar(30) COLLATE utf8_bin NOT NULL,
  `login` varchar(25) COLLATE utf8_bin NOT NULL,
  `mdp` varchar(75) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`idEtudiant`),
  KEY `FK_ETU_CLASSE` (`idClasse`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `etudiant`
--

INSERT INTO `etudiant` (`idEtudiant`, `idClasse`, `nom`, `prenom`, `mail`, `login`, `mdp`) VALUES
(1, 3, 'RISHARE', 'Matis', 'mrishare@la-joliverie.com', 'mrishare', 'd9047f8535bec0bdf4784f37bf9420760613e239'),
(2, 4, 'PHONTENO', 'Téheau', 'tphonteno@la-joliverie.com', 'tphonteno', 'ee36b7d311bba9fda45a18b018476fdf99c778b5');

-- --------------------------------------------------------

--
-- Structure de la table `pistestage`
--

DROP TABLE IF EXISTS `pistestage`;
CREATE TABLE IF NOT EXISTS `pistestage` (
  `idPisteStage` int(3) NOT NULL AUTO_INCREMENT,
  `idAnnee` int(3) NOT NULL,
  `idEntreprise` int(3) NOT NULL,
  `idEtudiant` int(3) NOT NULL,
  `idReferant` int(3) NOT NULL,
  `dateDeb` date NOT NULL,
  `dateFin` date NOT NULL,
  `contenu` text COLLATE utf8_bin NOT NULL,
  `etat` varchar(33) COLLATE utf8_bin NOT NULL,
  `situationActuelle` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`idPisteStage`),
  KEY `FK_PISTE_REF` (`idReferant`),
  KEY `FK_PISTE_ENTREPRISE` (`idEntreprise`),
  KEY `FK_PISTE_ETUDIANT` (`idEtudiant`),
  KEY `FK_LISTE_ANNE` (`idAnnee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `profprincipal`
--

DROP TABLE IF EXISTS `profprincipal`;
CREATE TABLE IF NOT EXISTS `profprincipal` (
  `idProfPrincipal` int(3) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) COLLATE utf8_bin NOT NULL,
  `prenom` varchar(25) COLLATE utf8_bin NOT NULL,
  `idClasse` int(3) NOT NULL,
  `idAnnee` int(3) NOT NULL,
  PRIMARY KEY (`idProfPrincipal`),
  KEY `FK_PRINCIPAL_ANNEE` (`idAnnee`),
  KEY `FK_PRINCIPAL_CLASSE` (`idClasse`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `profprincipal`
--

INSERT INTO `profprincipal` (`idProfPrincipal`, `nom`, `prenom`, `idClasse`, `idAnnee`) VALUES
(1, 'BOURGEOIS', 'Nicolas', 3, 2),
(2, 'CONTANT', 'Nelly', 4, 2);

-- --------------------------------------------------------

--
-- Structure de la table `rapport`
--

DROP TABLE IF EXISTS `rapport`;
CREATE TABLE IF NOT EXISTS `rapport` (
  `idRapport` int(3) NOT NULL AUTO_INCREMENT,
  `idEnseignant` int(3) NOT NULL,
  `idStage` int(3) NOT NULL,
  `bilan` text COLLATE utf8_bin NOT NULL,
  `resUtil` text COLLATE utf8_bin NOT NULL COMMENT 'ressources utilisées',
  `commentaires` text COLLATE utf8_bin NOT NULL,
  `appreciation` text COLLATE utf8_bin NOT NULL,
  `refParticipe` tinyint(1) NOT NULL,
  PRIMARY KEY (`idRapport`),
  KEY `FK_RAPPORT_ENSEIGNANT` (`idEnseignant`),
  KEY `FK_RAPPORT_STAGE` (`idStage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `referent`
--

DROP TABLE IF EXISTS `referent`;
CREATE TABLE IF NOT EXISTS `referent` (
  `idReferent` int(3) NOT NULL AUTO_INCREMENT,
  `idEntreprise` int(3) NOT NULL,
  `nom` varchar(25) COLLATE utf8_bin NOT NULL,
  `prenom` varchar(25) COLLATE utf8_bin NOT NULL,
  `mail` varchar(30) COLLATE utf8_bin NOT NULL,
  `telephone` varchar(10) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`idReferent`),
  KEY `FK_REF_ENTREPRISE` (`idEntreprise`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `referent`
--

INSERT INTO `referent` (`idReferent`, `idEntreprise`, `nom`, `prenom`, `mail`, `telephone`) VALUES
(1, 1, 'REF_RQL_NOM', 'REF_RQL_PRENOM', '', '0'),
(2, 2, 'REF_PSA_NOM', 'REF_PSA_PRENOM', '', '1111111111');

-- --------------------------------------------------------

--
-- Structure de la table `specialite`
--

DROP TABLE IF EXISTS `specialite`;
CREATE TABLE IF NOT EXISTS `specialite` (
  `idSpe` int(3) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(25) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`idSpe`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `specialite`
--

INSERT INTO `specialite` (`idSpe`, `libelle`) VALUES
(1, 'SLAM'),
(2, 'SISR');

-- --------------------------------------------------------

--
-- Structure de la table `stagedefinitif`
--

DROP TABLE IF EXISTS `stagedefinitif`;
CREATE TABLE IF NOT EXISTS `stagedefinitif` (
  `idStageDef` int(3) NOT NULL AUTO_INCREMENT,
  `idAnnee` int(3) NOT NULL,
  `idEntreprise` int(3) NOT NULL,
  `idEtudiant` int(3) NOT NULL,
  `idReferent` int(3) NOT NULL,
  `idEnseignant` int(3) NOT NULL,
  `dateDeb` date NOT NULL,
  `dateFin` date NOT NULL,
  `contenu` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`idStageDef`),
  KEY `FK_STAGE_REF` (`idReferent`),
  KEY `FK_STAGE_ENTREPRISE` (`idEntreprise`),
  KEY `FK_STAGE_ETUDIANT` (`idEtudiant`),
  KEY `FK_STAGE_ANNEE` (`idAnnee`),
  KEY `FK_STAGE_ENS` (`idEnseignant`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `stagedefinitif`
--

INSERT INTO `stagedefinitif` (`idStageDef`, `idAnnee`, `idEntreprise`, `idEtudiant`, `idReferent`, `idEnseignant`, `dateDeb`, `dateFin`, `contenu`) VALUES
(1, 2, 1, 1, 1, 1, '1999-01-04', '2040-02-19', ''),
(2, 2, 2, 2, 2, 2, '2001-01-04', '2023-02-19', '');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `classe`
--
ALTER TABLE `classe`
  ADD CONSTRAINT `FK_CLASSE_ANNEE` FOREIGN KEY (`idAnnee`) REFERENCES `annee` (`idAnnee`),
  ADD CONSTRAINT `FK_CLASSE_SPE` FOREIGN KEY (`idSpe`) REFERENCES `specialite` (`idSpe`);

--
-- Contraintes pour la table `etudiant`
--
ALTER TABLE `etudiant`
  ADD CONSTRAINT `FK_ETU_CLASSE` FOREIGN KEY (`idClasse`) REFERENCES `classe` (`idClasse`);

--
-- Contraintes pour la table `pistestage`
--
ALTER TABLE `pistestage`
  ADD CONSTRAINT `FK_LISTE_ANNE` FOREIGN KEY (`idAnnee`) REFERENCES `annee` (`idAnnee`),
  ADD CONSTRAINT `FK_PISTE_ENTREPRISE` FOREIGN KEY (`idEntreprise`) REFERENCES `entreprise` (`idEntreprise`),
  ADD CONSTRAINT `FK_PISTE_ETUDIANT` FOREIGN KEY (`idEtudiant`) REFERENCES `etudiant` (`idEtudiant`),
  ADD CONSTRAINT `FK_PISTE_REF` FOREIGN KEY (`idReferant`) REFERENCES `referent` (`idReferent`);

--
-- Contraintes pour la table `profprincipal`
--
ALTER TABLE `profprincipal`
  ADD CONSTRAINT `FK_PRINCIPAL_ANNEE` FOREIGN KEY (`idAnnee`) REFERENCES `annee` (`idAnnee`),
  ADD CONSTRAINT `FK_PRINCIPAL_CLASSE` FOREIGN KEY (`idClasse`) REFERENCES `classe` (`idClasse`);

--
-- Contraintes pour la table `rapport`
--
ALTER TABLE `rapport`
  ADD CONSTRAINT `FK_RAPPORT_ENSEIGNANT` FOREIGN KEY (`idEnseignant`) REFERENCES `enseignant` (`idEnseignant`),
  ADD CONSTRAINT `FK_RAPPORT_STAGE` FOREIGN KEY (`idStage`) REFERENCES `stagedefinitif` (`idStageDef`);

--
-- Contraintes pour la table `referent`
--
ALTER TABLE `referent`
  ADD CONSTRAINT `FK_REF_ENTREPRISE` FOREIGN KEY (`idEntreprise`) REFERENCES `entreprise` (`idEntreprise`);

--
-- Contraintes pour la table `stagedefinitif`
--
ALTER TABLE `stagedefinitif`
  ADD CONSTRAINT `FK_STAGE_ANNEE` FOREIGN KEY (`idAnnee`) REFERENCES `annee` (`idAnnee`),
  ADD CONSTRAINT `FK_STAGE_ENS` FOREIGN KEY (`idEnseignant`) REFERENCES `enseignant` (`idEnseignant`),
  ADD CONSTRAINT `FK_STAGE_ENTREPRISE` FOREIGN KEY (`idEntreprise`) REFERENCES `entreprise` (`idEntreprise`),
  ADD CONSTRAINT `FK_STAGE_ETUDIANT` FOREIGN KEY (`idEtudiant`) REFERENCES `etudiant` (`idEtudiant`),
  ADD CONSTRAINT `FK_STAGE_REF` FOREIGN KEY (`idReferent`) REFERENCES `referent` (`idReferent`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
