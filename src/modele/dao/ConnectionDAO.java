/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;
import java.sql.*;

/**
 *
 * @author Mathias
 */
public class ConnectionDAO 
{
    private static String url = "jdbc:mysql://localhost/gestionstage";
    private static String loginBD = "root";
    private static String mdpBD = "";
    private static Connection cnx = null;
    public static Connection connecter() throws SQLException {
    if (cnx == null) 
    {
       cnx = DriverManager.getConnection(url, loginBD, mdpBD);
    }
    return cnx;
    }
    public static void initialiser(String url, String login, String mdp)
    {
       ConnectionDAO.url = url;
       loginBD = login;
       mdpBD = mdp;
    }
}
