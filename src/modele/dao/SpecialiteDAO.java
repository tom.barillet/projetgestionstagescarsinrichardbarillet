/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modele.metier.Specialite;

/**
 *
 * @author tomba
 */
public class SpecialiteDAO {
    
    public static Specialite getOneById(int id) throws SQLException
   {
        Specialite uneSpecialite = null;
        Connection cnx = JDBC.connecter();
        String requete = "SELECT * FROM specialite WHERE idSpe = ?";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        String idS = String.valueOf(id);
        pstmt.setString(1, idS);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            String libelleSpe = rs.getString("libelle");
            uneSpecialite = new Specialite(id, libelleSpe);
        }
        return uneSpecialite;
   }
   
   public static ArrayList<Specialite> getAll() throws SQLException
   {
       ArrayList<Specialite> listeSpecialite = new ArrayList<Specialite>();
        Connection cnx = JDBC.connecter();
        String requete = "SELECT * FROM specialite";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            String id = String.valueOf(rs.getInt("idSpe"));
            String libelleSpe = rs.getString("libelle");
            Specialite uneSpecialite = new Specialite(Integer.parseInt(id), libelleSpe);
            listeSpecialite.add(uneSpecialite);
        }
        return listeSpecialite;
   }
    
}
