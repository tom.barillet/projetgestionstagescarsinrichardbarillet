/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import modele.metier.Annee;
import modele.metier.Entreprise;
import modele.metier.Etudiant;
import modele.metier.Referent;
import modele.metier.PisteStage;

/**
 *
 * @author Pol-Aurélien
 */
public class PisteStageDAO {
    
  public static PisteStage getOneById(int id) throws SQLException
    {
        
        PisteStage unePisteStage = null;
        Connection cnx = JDBC.connecter();
        // préparer la requête
        String requete = "SELECT * FROM pistestage WHERE idPisteStage = ?";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        String idS = String.valueOf(id);
        pstmt.setString(1, idS);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) { 
            Annee annee = AnneeDAO.getOneById(rs.getInt("idAnnee"));
            Entreprise entreprise = EntrepriseDAO.getOneById(rs.getInt("idEntreprise"));
            Etudiant etudiant = EtudiantDAO.getOneById(rs.getInt("idEtudiant"));
            Referent referent = ReferentDAO.getOneById(rs.getInt("idReferent"));
            Date dateDebut = rs.getDate("dateDeb");
            Date dateFin = rs.getDate("dateFin");
            String contenu;
            if(rs.getString("contenu") == null)
            {
                contenu = "";
            }
            else
            {
                contenu = rs.getString("contenu");
            }
            String etat = rs.getString("etat");
            String situationActuelle = rs.getString("situationActuelle");

            unePisteStage = new PisteStage(id, annee, entreprise, etudiant, referent, dateDebut, dateFin, contenu, etat, situationActuelle);
        }
        return unePisteStage;
    }
    
    public static ArrayList<PisteStage> getAllPisteStage() throws SQLException
    {
        ArrayList<PisteStage> listeStage = new ArrayList<PisteStage>();
        Connection cnx = JDBC.connecter();
        String requete = "SELECT * FROM stagedefinitif";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) { 
            String id = String.valueOf(rs.getInt("idStageDef"));
            Annee annee = AnneeDAO.getOneById(rs.getInt("idAnnee"));
            Entreprise entreprise = EntrepriseDAO.getOneById(rs.getInt("idEntreprise"));
            Etudiant etudiant = EtudiantDAO.getOneById(rs.getInt("idEtudiant"));
            Referent referent = ReferentDAO.getOneById(rs.getInt("idReferent"));
            Date dateDebut = rs.getDate("dateDeb");
            Date dateFin = rs.getDate("dateFin");
            String contenu;
            if(rs.getString("contenu") == null)
            {
                contenu = "";
            }
            else
            {
                contenu = rs.getString("contenu");
            }
            String etat = rs.getString("etat");
            String situationActuelle = rs.getString("situationActuelle");
            PisteStage unStage = new PisteStage(Integer.parseInt(id), annee, entreprise, etudiant, referent, dateDebut, dateFin, contenu,etat, situationActuelle);
            listeStage.add(unStage);
        }
        return listeStage;
        
    }
    
}
