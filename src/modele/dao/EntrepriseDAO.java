/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modele.metier.Annee;
import modele.metier.Entreprise;

/**
 *
 * @author tomba
 */
public class EntrepriseDAO {
    
    public static Entreprise getOneById(int id) throws SQLException
   {
        Entreprise uneEntreprise = null;
        Connection cnx = JDBC.connecter();
        String requete = "SELECT * FROM entreprise WHERE idEntreprise = ?";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        String idS = String.valueOf(id);
        pstmt.setString(1, idS);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            String nom = rs.getString("nom");
            String adresse = rs.getString("adresse");
            String raisonSociale = rs.getString("raisonSociale");
            uneEntreprise = new Entreprise(id, nom, adresse, raisonSociale);
        }
        return uneEntreprise;
   }
       public static ArrayList<Entreprise> getAll() throws SQLException
   {
       ArrayList<Entreprise> listeEntreprise = new ArrayList<Entreprise>();
        Connection cnx = JDBC.connecter();
        String requete = "SELECT * FROM entreprise";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            String id = String.valueOf(rs.getInt("idEntreprise"));
            String nom = rs.getString("nom");
            String adresse = rs.getString("adresse");
            String raisonSociale = rs.getString("raisonSociale");
            Entreprise uneEntreprise = new Entreprise(Integer.parseInt(id), nom, adresse, raisonSociale);
            listeEntreprise.add(uneEntreprise);
        }
        return listeEntreprise;
   }
   
}
