/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modele.metier.Classe;
import modele.metier.*;

/**
 *
 * @author tomba
 */
public class EnseignantDAO {
     public static Enseignant getOneById(int id) throws SQLException 
{
        Enseignant unEnseignant = null;
        Connection cnx = JDBC.connecter();
        // préparer la requête
        String requete = "SELECT * FROM Enseignant WHERE idEnseignant = ?";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        String idd = String.valueOf(id);
        pstmt.setString(1, idd);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            int idProf = rs.getInt("idEnseignant"); 
            String unNom = rs.getString("nom");
            String unPrenom = rs.getString("prenom");
            String unMail = rs.getString("mail");
            String unLogin = rs.getString("login");
            String unMdp = rs.getString("mdp");
            
            unEnseignant = new Enseignant(idProf,  unNom, unPrenom, unMail, unLogin, unMdp);
        }
        return unEnseignant;   
    
}}
