/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modele.metier.Classe;
import modele.metier.ProfPrincipal;

/**
 *
 * @author Mathias
 */
public class ProfPrincipalDAO {
    
    public static ProfPrincipal getOneById(int id) throws SQLException 
{
        ProfPrincipal unProfPrincipal = null;
        Connection cnx = JDBC.connecter();
        // préparer la requête
        String requete = "SELECT * FROM ProfPrincipal WHERE idProfPrincipal = ?";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        String idd = String.valueOf(id);
        pstmt.setString(1, idd);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            int idProf = rs.getInt("id"); 
            int idCla = Integer.parseInt(rs.getString("idClasse"));
            Classe uneClasse = ClasseDAO.getOneById(idCla);
            String unNom = rs.getString("nom");
            String unPrenom = rs.getString("prenom");
            String unMail = rs.getString("mail");
            String unLogin = rs.getString("login");
            String unMdp = rs.getString("mdp");
            
            unProfPrincipal = new ProfPrincipal(idProf,  unNom, unPrenom, unMail, unLogin, unMdp, uneClasse);
        }
        return unProfPrincipal;   
        
        
 
} 
    public static ProfPrincipal getOneByClasse(Classe classe) throws SQLException {
        ProfPrincipal unProfPrincipal = null;
        Connection cnx = JDBC.connecter();
        String requete = "SELECT * FROM ProfPrincipal WHERE idClasse = ?";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        int idClasse = classe.getIdClasse();
        pstmt.setInt(1, idClasse);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            int idProf = rs.getInt("id"); 
            int idCla = Integer.parseInt(rs.getString("idClasse"));
            Classe uneClasse = ClasseDAO.getOneById(idCla);
            String unNom = rs.getString("nom");
            String unPrenom = rs.getString("prenom");
            String unMail = rs.getString("mail");
            String unLogin = rs.getString("login");
            String unMdp = rs.getString("mdp");
            
            unProfPrincipal = new ProfPrincipal(idProf,  unNom, unPrenom, unMail, unLogin, unMdp, uneClasse);
        }
        return unProfPrincipal;
    }
    
}
