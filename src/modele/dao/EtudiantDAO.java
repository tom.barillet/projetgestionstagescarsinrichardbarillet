/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modele.metier.Etudiant;
import java.util.ArrayList;
import modele.metier.Classe;


/**
 *
 * @author Mathias
 */
public class EtudiantDAO {
    
    
    
public static Etudiant getOneById(int id) throws SQLException 
{
        Etudiant unEtudiant = null;
        Connection cnx = JDBC.connecter();
        // préparer la requête
        String requete = "SELECT * FROM ETUDIANT WHERE idEtudiant = ?";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        String idd = String.valueOf(id);
        pstmt.setString(1, idd);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            int idEtudiant = rs.getInt("idEtudiant"); 
            int idCla = Integer.parseInt(rs.getString("idClasse"));
            Classe uneClasse = ClasseDAO.getOneById(idCla);
            String unNom = rs.getString("nom");
            String unPrenom = rs.getString("prenom");
            String unMail = rs.getString("mail");
            String unLogin = rs.getString("login");
            String unMdp = rs.getString("mdp");
            
            unEtudiant = new Etudiant(idEtudiant,  unNom, unPrenom, unMail, unLogin, unMdp, uneClasse);
        }
        return unEtudiant;   
 
}    
    
public static ArrayList<Etudiant> getAll() throws SQLException
    {
        ArrayList<Etudiant> liste = new ArrayList();
        
        Connection cnx = JDBC.connecter();
        // préparer la requête
        String requete = "SELECT * FROM etudiant";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            int idEtudiant = rs.getInt("idEtudiant"); 
            int idCla = Integer.parseInt(rs.getString("idClasse"));
            Classe uneClasse = ClasseDAO.getOneById(idCla);
            String unNom = rs.getString("nom");
            String unPrenom = rs.getString("prenom");
            String unMail = rs.getString("mail");
            String unLogin = rs.getString("login");
            String unMdp = rs.getString("mdp");
            Etudiant unEtudiant = new Etudiant(idEtudiant, unNom, unPrenom, unMail, unLogin, unMdp, uneClasse);
            liste.add(unEtudiant);
        }
        return liste;
    } 
    










}
