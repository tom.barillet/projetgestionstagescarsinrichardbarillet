/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;
import modele.metier.Annee;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author Pol-Aurélien
 */
public class AnneeDAO 
{
   public static Annee getOneById(int id) throws SQLException
   {
        Annee uneAnnee = null;
        Connection cnx = JDBC.connecter();
        String requete = "SELECT * FROM annee WHERE idAnnee = ?";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        String idS = String.valueOf(id);
        pstmt.setString(1, idS);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            String annee = rs.getString("annee");
            uneAnnee = new Annee(id, annee);
        }
        return uneAnnee;
   }
   
   public static ArrayList<Annee> getAll() throws SQLException
   {
       ArrayList<Annee> listeAnnee = new ArrayList<Annee>();
        Connection cnx = JDBC.connecter();
        String requete = "SELECT * FROM annee";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            String id = String.valueOf(rs.getInt("idAnnee"));
            String annee = rs.getString("annee");
            Annee uneAnnee = new Annee(Integer.parseInt(id), annee);
            listeAnnee.add(uneAnnee);
        }
        return listeAnnee;
   }
}
