/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;

import modele.metier.Referent;
import modele.metier.Entreprise;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author Pol-Aurélien
 */
public class ReferentDAO 
{
    public static Referent getOneById(int id) throws SQLException
   {
        Referent unReferent = null;
        Connection cnx = JDBC.connecter();
        String requete = "SELECT * FROM referent WHERE idReferent = ?";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        String idS = String.valueOf(id);
        pstmt.setString(1, idS);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            Entreprise entreprise = EntrepriseDAO.getOneById(rs.getInt("idEntreprise"));
            String nom = rs.getString("nom");
            String prenom = rs.getString("prenom");
            String mail = rs.getString("mail");
            String telephone = rs.getString("telephone");
            unReferent = new Referent(id, entreprise, nom, prenom, mail, telephone);
        }
        return unReferent;
   }
   
   public static ArrayList<Referent> getAll() throws SQLException
   {
        ArrayList<Referent> listeReferent = new ArrayList<Referent>();
        Connection cnx = JDBC.connecter();
        String requete = "SELECT * FROM referent";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            Entreprise entreprise = EntrepriseDAO.getOneById(rs.getInt("idEntreprise"));
            String id = rs.getString("idReferent");
            String nom = rs.getString("nom");
            String prenom = rs.getString("prenom");
            String mail = rs.getString("mail");
            String telephone = rs.getString("telephone");
            Referent unReferent = new Referent(Integer.parseInt(id),entreprise, nom, prenom, mail, telephone);
            listeReferent.add(unReferent);
        }
        return listeReferent;
   }
}
