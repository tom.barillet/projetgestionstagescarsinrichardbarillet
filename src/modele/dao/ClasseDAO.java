/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modele.metier.Classe;
import modele.metier.Annee;
import modele.metier.Specialite;

/**
 *
 * @author Mathias
 */
public class ClasseDAO {
    public static Classe getOneById(int id) throws SQLException 
{
        Connection cnx = JDBC.connecter();
        // préparer la requête
        String requete = "SELECT * FROM CLASSE WHERE idClasse = ?";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        String idd = String.valueOf(id);
        pstmt.setString(1, idd);
        ResultSet rs = pstmt.executeQuery();
        Classe laClasse = null;
        if (rs.next()) {
            int idClasse = rs.getInt("idClasse"); 
            String unNom = rs.getString("nom");
            Annee uneAnnee = AnneeDAO.getOneById(rs.getInt("idAnnee"));
            int idSpe = Integer.parseInt(rs.getString("idSpe"));
            Specialite uneSpecialite = SpecialiteDAO.getOneById(idSpe);
            laClasse = new Classe(idClasse, uneSpecialite, uneAnnee, unNom);
        }
        return laClasse;   
 
}    
    
public static ArrayList<Classe> getAll() throws SQLException
    {
        ArrayList<Classe> listeClasse = new ArrayList();
        Connection cnx = JDBC.connecter();
        // préparer la requête
        String requete = "SELECT * FROM classe";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            int idClasse = rs.getInt("idClasse");
            String unNom = rs.getString("nom");
            Annee uneAnnee = AnneeDAO.getOneById(rs.getInt("idAnnee"));
            int idSpe = Integer.parseInt(rs.getString("idSpe"));
            Specialite uneSpecialite = SpecialiteDAO.getOneById(idSpe);
            Classe uneClasse = new Classe(idClasse, uneSpecialite, uneAnnee, unNom);
            listeClasse.add(uneClasse);
        }
        return listeClasse;
    } 
    
}
