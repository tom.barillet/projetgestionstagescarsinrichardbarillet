/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;
import java.sql.*;
import modele.metier.*;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Pol-Aurélien
 */
public class StageDAO {
    
    public static StageDefinitif getOneById(int id) throws SQLException
    {
        StageDefinitif unStage = null;
        Connection cnx = JDBC.connecter();
        // préparer la requête
        String requete = "SELECT * FROM stagedefinitif WHERE idStageDef = ?";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        String idS = String.valueOf(id);
        pstmt.setString(1, idS);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) { 
            Annee annee = AnneeDAO.getOneById(rs.getInt("idAnnee"));
            Entreprise entreprise = EntrepriseDAO.getOneById(rs.getInt("idEntreprise"));
            Etudiant etudiant = EtudiantDAO.getOneById(rs.getInt("idEtudiant"));
            Referent referent = ReferentDAO.getOneById(rs.getInt("idReferent"));
            Enseignant enseignant = EnseignantDAO.getOneById(rs.getInt("idEnseignant"));
            Date dateDebut = rs.getDate("dateDeb");
            Date dateFin = rs.getDate("dateFin");
            String contenu;
            if(rs.getString("contenu") == null)
            {
                contenu = "";
            }
            else
            {
                contenu = rs.getString("contenu");
            }
            unStage = new StageDefinitif(id, annee, entreprise, etudiant, referent, enseignant, dateDebut, dateFin, contenu);
        }
        return unStage;
    }
    
    public static ArrayList<StageDefinitif> getAllStageDef() throws SQLException
    {
        ArrayList<StageDefinitif> listeStage = new ArrayList<StageDefinitif>();
        Connection cnx = JDBC.connecter();
        String requete = "SELECT * FROM stagedefinitif";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) { 
            String id = String.valueOf(rs.getInt("idStageDef"));
            Annee annee = AnneeDAO.getOneById(rs.getInt("idAnnee"));
            Entreprise entreprise = EntrepriseDAO.getOneById(rs.getInt("idEntreprise"));
            Etudiant etudiant = EtudiantDAO.getOneById(rs.getInt("idEtudiant"));
            Referent referent = ReferentDAO.getOneById(rs.getInt("idReferent"));
            Enseignant enseignant = EnseignantDAO.getOneById(rs.getInt("idEnseignant"));
            Date dateDebut = rs.getDate("dateDeb");
            Date dateFin = rs.getDate("dateFin");
            String contenu;
            if(rs.getString("contenu") == null)
            {
                contenu = "";
            }
            else
            {
                contenu = rs.getString("contenu");
            }
            StageDefinitif unStage = new StageDefinitif(Integer.parseInt(id), annee, entreprise, etudiant, referent, enseignant, dateDebut, dateFin, contenu);
            listeStage.add(unStage);
        }
        return listeStage;
        
    }
    
    public static ArrayList<StageDefinitif> getAllStageDefActuels(String classe, String eleve_nom, String eleve_prenom, String prof_nom, String prof_prenom, String annee ) throws SQLException
    {
        ArrayList<StageDefinitif> listeStage = new ArrayList<StageDefinitif>();
        Connection cnx = JDBC.connecter();
        String baseRequete = "SELECT SD.idStageDef, A.idAnnee, E.idEntreprise, Etu.idEtudiant, R.idReferent, En.idEnseignant, SD.contenu, C.nom, Etu.nom, Etu.prenom, E.nom, SD.dateDeb, SD.dateFin, En.nom, En.prenom, R.nom, R.prenom, A.annee FROM stagedefinitif SD INNER JOIN etudiant Etu On Etu.idEtudiant=SD.idEtudiant INNER JOIN enseignant En ON EN.idEnseignant = SD.idEnseignant INNER JOIN entreprise E ON E.idEntreprise = SD.idEntreprise INNER JOIN referent R ON R.idReferent = SD.idReferent INNER JOIN classe C ON C.idClasse = Etu.idClasse INNER JOIN annee A ON A.idAnnee = C.idAnnee WHERE DATE(NOW()) BETWEEN dateDeb AND dateFin";
        
        String requeteClasse = " AND C.nom LIKE \"%"+classe+"%\"";
        String requeteEtuPre = " AND Etu.prenom LIKE \"%"+ eleve_prenom+"%\"";
        String requeteEtuNom = " AND Etu.nom LIKE \"%"+eleve_nom+"%\"";
        String requeteProfPre = " AND En.prenom LIKE \"%"+ prof_prenom+"%\"" ;
        String requeteProfNom = " AND En.nom LIKE \"%"+prof_nom+"%\"";
        String requeteAnnee = " AND A.annee LIKE \"%"+annee+"%\"";
        if (classe != null) {
            baseRequete += requeteClasse;
        }
        if (!"".equals(eleve_nom)){
            baseRequete += requeteEtuNom;
        }
        if (!"".equals(eleve_prenom)){
            baseRequete += requeteEtuPre;
        }
        if (!"".equals(prof_nom)){
            baseRequete += requeteProfNom;
        }
        if (!"".equals(prof_prenom)){
            baseRequete += requeteProfPre;
        }
        if (annee != null ){
            baseRequete += requeteAnnee; 
        }
        
        PreparedStatement pstmt = cnx.prepareStatement(baseRequete);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) { 
            String id = String.valueOf(rs.getInt("idStageDef"));
            Annee annee1 = AnneeDAO.getOneById(rs.getInt("idAnnee"));
            Entreprise entreprise = EntrepriseDAO.getOneById(rs.getInt("idEntreprise"));
            Etudiant etudiant = EtudiantDAO.getOneById(rs.getInt("idEtudiant"));
            Referent referent = ReferentDAO.getOneById(rs.getInt("idReferent"));
            Enseignant enseignant = EnseignantDAO.getOneById(rs.getInt("idEnseignant"));
            Date dateDebut = rs.getDate("dateDeb");
            Date dateFin = rs.getDate("dateFin");
            String contenu;
            if(rs.getString("contenu") == null)
            {
                contenu = "";
            }
            else
            {
                contenu = rs.getString("contenu");
            }
            StageDefinitif unStage = new StageDefinitif(Integer.parseInt(id), annee1, entreprise, etudiant, referent, enseignant, dateDebut, dateFin, contenu);
            listeStage.add(unStage);
        }
        return listeStage;
        
    }
    
}
