/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.metier;

/**
 *
 * @author tomba
 */
public class Etudiant extends Utilisateur {
    

    private Classe Classe;

    public Etudiant(int unId, String unNom, String unPrenom, String unMail,String unLogin, String unMdp, Classe laClasse) {
        super(unId, unNom, unPrenom, unMail, unLogin, unMdp);
        
        Classe = laClasse;
    }
    

    public int getIdEtudiant() {
        return id;
    }

    public void setIdEtudiant(int idEtudiant) {
        this.id = idEtudiant;
    }

    public Classe getLaClasse() {
        return Classe;
    }

    public void setLaClasse(Classe laClasse) {
        this.Classe = laClasse;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }
    
}
