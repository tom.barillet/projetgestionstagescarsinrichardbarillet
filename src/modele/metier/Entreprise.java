/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.metier;

/**
 *
 * @author tomba
 */
public class Entreprise {
    private int id;
    private String nom;
    private String adresse;
    private String raisonSociale;
    
    public Entreprise(int unId, String unNom, String uneAdresse, String uneRaisonSociale){
        id = unId;
        nom = unNom;
        adresse = uneAdresse;
        raisonSociale = uneRaisonSociale;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getRaisonSociale() {
        return raisonSociale;
    }

    public void setRaisonSociale(String raisonSociale) {
        this.raisonSociale = raisonSociale;
    }
    
}
