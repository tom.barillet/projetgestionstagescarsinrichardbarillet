/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.metier;

/**
 *
 * @author tomba
 */
public class Referent extends Personne
{
    private Entreprise lEntreprise;
    private String telephone;
    
    public Referent( int unIdReferent, Entreprise uneEntreprise, String unNom, String unPrenom, String unMail, String unTelephone){
        super(unIdReferent, unNom, unPrenom, unMail);
        lEntreprise = uneEntreprise;
        telephone = unTelephone;
    }

    public int getIdReferent() {
        return id;
    }

    public void setIdReferent(int idReferent) {
        this.id = idReferent;
    }

    public Entreprise getlEntreprise() {
        return lEntreprise;
    }

    public void setlEntreprise(Entreprise lEntreprise) {
        this.lEntreprise = lEntreprise;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
        
    
}
