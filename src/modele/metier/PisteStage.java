/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.metier;
import java.util.Date;
/**
 *
 * @author tomba
 */
public class PisteStage {
    
    private int idPisteStage;
    private Annee lAnnee;
    private Entreprise lEntreprise;
    private Etudiant lEtudiant;
    private Referent leReferent;
    private Date dateDeb;
    private Date dateFin;
    private String contenu;
    private String etat;
    private String situationActuelle;
    
    public PisteStage(int unId, Annee uneAnnee, Entreprise uneEntreprise, Etudiant unEtudiant, Referent unReferent, Date uneDateDebut, Date uneDateFin, String unContenu, String unEtat, String uneSituation){
        idPisteStage = unId;
        lAnnee = uneAnnee;
        lEntreprise = uneEntreprise;
        lEtudiant = unEtudiant;
        leReferent = unReferent;
        dateDeb = uneDateDebut;
        dateFin = uneDateFin;
        contenu = unContenu;
        etat = unEtat;
        situationActuelle = uneSituation;
    }

    public int getIdPisteStage() {
        return idPisteStage;
    }

    public void setIdPisteStage(int idPisteStage) {
        this.idPisteStage = idPisteStage;
    }

    public Annee getlAnnee() {
        return lAnnee;
    }

    public void setlAnnee(Annee lAnnee) {
        this.lAnnee = lAnnee;
    }

    public Entreprise getlEntreprise() {
        return lEntreprise;
    }

    public void setlEntreprise(Entreprise lEntreprise) {
        this.lEntreprise = lEntreprise;
    }

    public Etudiant getlEtudiant() {
        return lEtudiant;
    }

    public void setlEtudiant(Etudiant lEtudiant) {
        this.lEtudiant = lEtudiant;
    }

    public Referent getLeReferent() {
        return leReferent;
    }

    public void setLeReferent(Referent leReferent) {
        this.leReferent = leReferent;
    }

    public Date getDateDeb() {
        return dateDeb;
    }

    public void setDateDeb(Date dateDeb) {
        this.dateDeb = dateDeb;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String estAccepte) {
        this.etat = estAccepte;
    }

    public String getSituationActuelle() {
        return situationActuelle;
    }

    public void setSituationActuelle(String situationActuelle) {
        this.situationActuelle = situationActuelle;
    }
    
}
