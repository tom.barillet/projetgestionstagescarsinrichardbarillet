/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.metier;

/**
 *
 * @author Mathias
 */
public class Rapport {
    
    int idRapport;
    Enseignant enseignant;
    String bilan;
    String ressourcesUtilisee;
    String commentaire;
    String appreciation;
    boolean referentParticipe;
    
    
    public Rapport (int unId, Enseignant unEnseignant, String unBilan, String uneRessource, String unComm, String uneAppreciation, boolean unReferent){
        idRapport = unId;
        enseignant = unEnseignant;
        bilan = unBilan;
        ressourcesUtilisee = uneRessource;
        commentaire = unComm;
        appreciation = uneAppreciation;
        referentParticipe = unReferent;
        
    }

    public int getIdRapport() {
        return idRapport;
    }

    public void setIdRapport(int idRapport) {
        this.idRapport = idRapport;
    }

    public Enseignant getEnseignant() {
        return enseignant;
    }

    public void setEnseignant(Enseignant enseignant) {
        this.enseignant = enseignant;
    }

    public String getBilan() {
        return bilan;
    }

    public void setBilan(String bilan) {
        this.bilan = bilan;
    }

    public String getRessourcesUtilisee() {
        return ressourcesUtilisee;
    }

    public void setRessourcesUtilisee(String ressourcesUtilisee) {
        this.ressourcesUtilisee = ressourcesUtilisee;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public String getAppreciation() {
        return appreciation;
    }

    public void setAppreciation(String appreciation) {
        this.appreciation = appreciation;
    }

    public boolean isReferentParticipe() {
        return referentParticipe;
    }

    public void setReferentParticipe(boolean referentParticipe) {
        this.referentParticipe = referentParticipe;
    }
    
    
}
