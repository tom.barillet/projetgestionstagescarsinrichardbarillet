/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.metier;

/**
 *
 * @author tomba
 */
public class Specialite {
    private int idSpe;
    private String libelleSpe;
    
    public Specialite(int unIdSpe, String unLibelleSpe){
        idSpe = unIdSpe;
        libelleSpe = unLibelleSpe;
    }

    public int getIdSpe() {
        return idSpe;
    }

    public void setIdSpe(int idSpe) {
        this.idSpe = idSpe;
    }

    public String getLibelleSpe() {
        return libelleSpe;
    }

    public void setLibelleSpe(String libelleSpe) {
        this.libelleSpe = libelleSpe;
    }
    
}
