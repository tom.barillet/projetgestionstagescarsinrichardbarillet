/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.metier;

/**
 *
 * @author Pol-Aurélien
 */
public class ProfPrincipal extends Enseignant {
    Classe classe;
    
    public ProfPrincipal(int unId, String unNom, String unPrenom, String unMail, String unLogin, String unMdp, Classe uneClasse) {
        super(unId, unNom, unPrenom, unMail, unLogin, unMdp);
        classe = uneClasse;
    }
    
}
