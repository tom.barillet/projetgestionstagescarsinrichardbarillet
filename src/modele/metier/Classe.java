/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.metier;

/**
 *
 * @author tomba
 */
public class Classe {
    private int idClasse;
    private Specialite laSpecialite;
    private Annee annee;
    private String nom;
    
    public Classe(int unIdClasse, Specialite uneSpecialite, Annee uneAnnee, String unNom){
        idClasse = unIdClasse;
        laSpecialite = uneSpecialite;
        annee = uneAnnee;
        nom = unNom;
    }

    public int getIdClasse() {
        return idClasse;
    }

    public void setIdClasse(int idClasse) {
        this.idClasse = idClasse;
    }

    public Specialite getLaSpecialite() {
        return laSpecialite;
    }

    public void setLaSpecialite(Specialite laSpecialite) {
        this.laSpecialite = laSpecialite;
    }

    public Annee getAnnee() {
        return annee;
    }

    public void setAnnee(Annee annee) {
        this.annee = annee;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
}
