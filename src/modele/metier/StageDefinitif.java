package modele.metier;

import java.util.Date;
import modele.metier.*;

public class StageDefinitif {
    
    private int idStageDef;
    private Annee lAnnee;
    private Entreprise lEntreprise;
    private Etudiant lEtudiant;
    private Referent leReferent;
    private Date dateDeb;
    private Date dateFin;
    private String contenu;
    private Enseignant lenseignant;

    
    public StageDefinitif(int unId, Annee uneAnnee, Entreprise uneEntreprise, Etudiant unEtudiant, Referent unReferent, Enseignant unEnseignant, Date uneDateDebut, Date uneDateFin, String unContenu){
        idStageDef = unId;
        lAnnee = uneAnnee;
        lEntreprise = uneEntreprise;
        lEtudiant = unEtudiant;
        leReferent = unReferent;
        lenseignant = unEnseignant;
        dateDeb = uneDateDebut;
        dateFin = uneDateFin;
        contenu = unContenu;
     
    }

    public int getIdStageDef() {
        return idStageDef;
    }

    public void setIdStageDef(int idPisteStage) {
        this.idStageDef = idPisteStage;
    }

    public Annee getlAnnee() {
        return lAnnee;
    }

    public void setlAnnee(Annee lAnnee) {
        this.lAnnee = lAnnee;
    }

    public Entreprise getlEntreprise() {
        return lEntreprise;
    }

    public void setlEntreprise(Entreprise lEntreprise) {
        this.lEntreprise = lEntreprise;
    }

    public Etudiant getlEtudiant() {
        return lEtudiant;
    }

    public void setlEtudiant(Etudiant lEtudiant) {
        this.lEtudiant = lEtudiant;
    }

    public Referent getLeReferent() {
        return leReferent;
    }

    public void setLeReferent(Referent leReferent) {
        this.leReferent = leReferent;
    }

    public Date getDateDeb() {
        return dateDeb;
    }

    public void setDateDeb(Date dateDeb) {
        this.dateDeb = dateDeb;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public Enseignant getLenseignant() {
        return lenseignant;
    }

    public void setLenseignant(Enseignant lenseignant) {
        this.lenseignant = lenseignant;
    }
    
    
}